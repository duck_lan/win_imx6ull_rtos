/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-20     liuduanfei   the first version
 */

#include <board.h>
#include "sd.h"
#include <board.h>
#include <rthw.h>
#include <rtdevice.h>
#include <board.h>



void init_pins(void)
{
	/* led pins */
    IOMUXC_SetPinMux(IOMUXC_SNVS_SNVS_TAMPER3_GPIO5_IO03, 0U);
    IOMUXC_SetPinConfig(IOMUXC_SNVS_SNVS_TAMPER3_GPIO5_IO03, 
                        IOMUXC_SW_PAD_CTL_PAD_SRE_MASK |
                        IOMUXC_SW_PAD_CTL_PAD_DSE(1U) |
                        IOMUXC_SW_PAD_CTL_PAD_HYS_MASK);

	/* KEY1 pins GPIO5_1 SNVS_TAMPER1 */
	IOMUXC_SetPinMux(IOMUXC_SNVS_SNVS_TAMPER1_GPIO5_IO01, 0U);
	IOMUXC_SetPinConfig(IOMUXC_SNVS_SNVS_TAMPER1_GPIO5_IO01, 
						IOMUXC_SW_PAD_CTL_PAD_DSE(6U) |
						IOMUXC_SW_PAD_CTL_PAD_SPEED(2U) |
						IOMUXC_SW_PAD_CTL_PAD_PKE_MASK |
						IOMUXC_SW_PAD_CTL_PAD_HYS_MASK);

	/* KEY2 pins GPIO4_IO14 */
	IOMUXC_SetPinMux(IOMUXC_NAND_CE1_B_GPIO4_IO14, 0U);
	IOMUXC_SetPinConfig(IOMUXC_NAND_CE1_B_GPIO4_IO14, 
						IOMUXC_SW_PAD_CTL_PAD_DSE(6U) |
						IOMUXC_SW_PAD_CTL_PAD_SPEED(2U) |
						IOMUXC_SW_PAD_CTL_PAD_PKE_MASK |
						IOMUXC_SW_PAD_CTL_PAD_HYS_MASK);
}

static void key_gpio5_handle_irq(int irqno, void *param)
{
	/* read GPIO5_DR to get GPIO5_IO01 status*/
	if((GPIO5->DR >> 1 ) & 0x1) {
		rt_kprintf("key 1 is release\r\n");
		/* led off, set GPIO5_DR to configure GPIO5_IO03 output 1 */
		GPIO5->DR |= (1<<3); //led on
	} else {
		rt_kprintf("key 1 is press\r\n");
		/* led on, set GPIO5_DR to configure GPIO5_IO03 output 0 */
		GPIO5->DR &= ~(1<<3); //led off
	}
	/* write 1 to clear GPIO5_IO03 interrput status*/
	GPIO5->ISR |= (1 << 1);
}

void key_gpio4_handle_irq(int irqno, void *param)
{
	/* read GPIO4_DR to get GPIO4_IO014 status*/
	if((GPIO4->DR >> 14 ) & 0x1)
		rt_kprintf("key 2 is release\r\n");
	else
		rt_kprintf("key 2 is press\r\n");
	/* write 1 to clear GPIO4_IO014 interrput status*/
	GPIO4->ISR |= (1 << 14);
}


void key_irq_init(void)
{
	/* if set detects any edge on the corresponding input signal*/
	GPIO5->EDGE_SEL |= (1 << 1);
	/* if set 1, unmasked, Interrupt n is enabled */
	GPIO5->IMR |= (1 << 1);
	/* clear interrupt first to avoid unexpected event */
	GPIO5->ISR |= (1 << 1);

	GPIO4->EDGE_SEL |= (1 << 14);
	GPIO4->IMR |= (1 << 14);
	GPIO4->ISR |= (1 << 14);

	rt_hw_interrupt_install(GPIO5_Combined_0_15_IRQn, key_gpio5_handle_irq, "aaaa", "aaaa");
	rt_hw_interrupt_install(GPIO4_Combined_0_15_IRQn, key_gpio4_handle_irq, "bbbbb", "bbbbb");
	rt_hw_interrupt_umask(GPIO5_Combined_0_15_IRQn);
	rt_hw_interrupt_umask(GPIO4_Combined_0_15_IRQn);

	rt_kprintf("rt_hw_interrupt_install   finish\r\n");
	// request_irq(GPIO5_Combined_0_15_IRQn, (irq_handler_t)key_gpio5_handle_irq, NULL);
	// request_irq(GPIO4_Combined_0_15_IRQn, (irq_handler_t)key_gpio4_handle_irq, NULL);
}


void system_init()
{
	init_pins();
	key_irq_init();
	// gic_enable_irq(GPIO5_Combined_0_15_IRQn);
	// gic_enable_irq(GPIO4_Combined_0_15_IRQn);
}

//component层定义了一个main,就调用到这里
int main(void)
{
    IOMUXC_SetPinMux(IOMUXC_SNVS_SNVS_TAMPER3_GPIO5_IO03, 0U);
	
    GPIO5->GDIR |= (1<<3);
	rt_kprintf("3333333333333\r\n");
	system_init();
	while(1)
	{
		rt_kprintf("aaaaaaaaaaaaaaaaaaaa\r\n");
		GPIO5->DR &= ~(1<<3);
		rt_thread_mdelay(1000);
		GPIO5->DR |= (1<<3);
		rt_thread_mdelay(1000);
	}

	return 0;
}


